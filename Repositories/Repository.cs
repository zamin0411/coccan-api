﻿using coccan_api.Models;
using Microsoft.EntityFrameworkCore;

namespace coccan_api.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, new()
    {
        protected readonly CoccanContext _context;

        public Repository(CoccanContext context)
        {
            _context = context;
        }

        public DbSet<TEntity> Get()
        {
            return _context.Set<TEntity>();
        }

        public TEntity? Get(Guid id)
        {
            return _context.Set<TEntity>().Find(id);    
        }



        public async Task SetAsync(TEntity entity)
        {
            _context.Update(entity);
            await _context.SaveChangesAsync();
        }
    }
}
