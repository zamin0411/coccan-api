﻿using coccan_api.Models;
using Microsoft.EntityFrameworkCore;

namespace coccan_api.Repositories
{

    public interface IGroupRepository : IRepository<Group>
    {
        Task<IEnumerable<Group>> GetAsync(string? search);
    }
    public class GroupRepository : Repository<Group>, IGroupRepository
    {
        private readonly CoccanContext _context;
        public GroupRepository(CoccanContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Group>> GetAsync(string? search)
        {
            if (search == null || search == "")
                return await GetAsync();
            return await Get().Include(g => g.Lecturer.Department)
                .Where(g => EF.Functions.Like(g.Lecturer.Username, $"%{search}%") || EF.Functions.Like(g.Name, $"%{search}%"))
                .Include(g => g.Lecturer.University)
                .Include(g => g.Messages)
                .Include(g => g.GroupStudents)
                .ThenInclude(g => g.Student)
                .ToListAsync();
        }

        public async Task<IEnumerable<Group>> GetAsync()
        {
            return await Get().Include(g => g.Lecturer.Department).Include(g => g.Lecturer.University).Include(g => g.Messages).Include(g => g.GroupStudents).ThenInclude(g => g.Student).ToListAsync();
        }
    }


}
