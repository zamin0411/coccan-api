﻿using Castle.Components.DictionaryAdapter.Xml;
using coccan_api.Models;
using Microsoft.EntityFrameworkCore;

namespace coccan_api.Repositories
{
    public interface IVoucherRepository : IRepository<Voucher>
    {
        Task<IEnumerable<Voucher>> GetAsync(DateTime? after, int size);
        Task<IEnumerable<Voucher>> GetAsync(int size);

        Task<IEnumerable<Voucher>> GetRelatedDataAsync();
    }

    public class VoucherRepository : Repository<Voucher>, IVoucherRepository
    {
        public VoucherRepository(CoccanContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Voucher>> GetRelatedDataAsync()
        {
            return await Get().Include(v => v.Organization).Include(v => v.Category).ToListAsync();
        }


        public async Task<IEnumerable<Voucher>> GetAsync(DateTime? after, int size)
        {
            if (after == null)
                return await GetAsync(size);
            return await Get().OrderBy(v => v.CreatedDate).Where(v => v.CreatedDate > after).Take(size).Include(v => v.Organization).Include(v => v.Category).ToListAsync();
        }

        public async Task<IEnumerable<Voucher>> GetAsync(int size)
        {
            return await Get().OrderBy(v => v.CreatedDate).Take(size).Include(v => v.Organization).Include(v => v.Category).ToListAsync();
        }
    }
}
