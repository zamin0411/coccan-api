﻿using AutoMapper;
using coccan_api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace coccan_api.Repositories
{

    public interface IUserRepository : IRepository<User>
    {
        Task<Guid?> AddAsync(User user);
        Task<IEnumerable<User>> GetAsync();
    }
    public class UserRepository : Repository<User>, IUserRepository
    {
        private readonly CoccanContext _context;
        public UserRepository(CoccanContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<User>> GetAsync()
        {
            
            return await _context.Users.Include(u => u.Department).Include(u => u.University).Include(u => u.Wallets).ToListAsync();
        }

        public async Task<User?> GetAsync(Guid id)
        {
            return await _context.Users.Where(u => u.Id == id).Include(u => u.Department).Include(u => u.University).Include(u => u.Wallets).SingleOrDefaultAsync();
        }

        public async Task<Guid?> AddAsync(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return user.Id;

        }
    }
}
