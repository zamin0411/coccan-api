﻿using Microsoft.EntityFrameworkCore;

namespace coccan_api.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        DbSet<TEntity> Get();
        TEntity? Get(Guid Id);
        Task SetAsync(TEntity entity);

    }
}
