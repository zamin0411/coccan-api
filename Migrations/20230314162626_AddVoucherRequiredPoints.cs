﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace coccanapi.Migrations
{
    /// <inheritdoc />
    public partial class AddVoucherRequiredPoints : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb3");

            migrationBuilder.CreateTable(
                name: "category",
                columns: table => new
                {
                    id = table.Column<uint>(type: "int unsigned", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "department",
                columns: table => new
                {
                    id = table.Column<uint>(type: "int unsigned", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "text", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "device_token",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    code = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "organization",
                columns: table => new
                {
                    id = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "varchar(95)", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3"),
                    logo = table.Column<string>(type: "text", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3"),
                    address = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3"),
                    description = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "university",
                columns: table => new
                {
                    id = table.Column<uint>(type: "int(10) unsigned", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "text", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "voucher",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    expireddate = table.Column<DateTime>(name: "expired_date", type: "datetime", nullable: false),
                    number = table.Column<uint>(type: "int unsigned", nullable: true),
                    address = table.Column<string>(type: "text", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3"),
                    organizationid = table.Column<ulong>(name: "organization_id", type: "bigint(20) unsigned", nullable: true),
                    description = table.Column<string>(type: "text", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3"),
                    categoryid = table.Column<uint>(name: "category_id", type: "int unsigned", nullable: true),
                    createddate = table.Column<DateTime>(name: "created_date", type: "datetime(6)", nullable: false),
                    requiredpoints = table.Column<uint>(name: "required_points", type: "int unsigned", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id);
                    table.ForeignKey(
                        name: "voucher_category_null_fk",
                        column: x => x.categoryid,
                        principalTable: "category",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "voucher_organization_null_fk",
                        column: x => x.organizationid,
                        principalTable: "organization",
                        principalColumn: "id");
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "department_university",
                columns: table => new
                {
                    departmentid = table.Column<uint>(name: "department_id", type: "int unsigned", nullable: false),
                    universityid = table.Column<uint>(name: "university_id", type: "int(10) unsigned", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => new { x.departmentid, x.universityid })
                        .Annotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                    table.ForeignKey(
                        name: "department_university_department_null_fk",
                        column: x => x.departmentid,
                        principalTable: "department",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "department_university_university_null_fk",
                        column: x => x.universityid,
                        principalTable: "university",
                        principalColumn: "id");
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    username = table.Column<string>(type: "varchar(95)", nullable: false, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3"),
                    email = table.Column<string>(type: "varchar(95)", nullable: false, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3"),
                    profilephoto = table.Column<string>(name: "profile_photo", type: "longtext", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3"),
                    departmentid = table.Column<uint>(name: "department_id", type: "int(10) unsigned", nullable: false, defaultValue: 1u),
                    universityid = table.Column<uint>(name: "university_id", type: "int(10) unsigned", nullable: false, defaultValue: 1u),
                    role = table.Column<string>(type: "longtext", nullable: false, defaultValue: "STUDENT", collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id);
                    table.ForeignKey(
                        name: "user_department_null_fk",
                        column: x => x.departmentid,
                        principalTable: "department",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "user_university_null_fk",
                        column: x => x.universityid,
                        principalTable: "university",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "campaign",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    lecturerid = table.Column<Guid>(name: "lecturer_id", type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    pointsgiven = table.Column<uint>(name: "points_given", type: "int unsigned", nullable: false),
                    participantslimit = table.Column<uint>(name: "participants_limit", type: "int unsigned", nullable: false),
                    expireddate = table.Column<DateTime>(name: "expired_date", type: "datetime", nullable: false),
                    target = table.Column<string>(type: "text", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id);
                    table.ForeignKey(
                        name: "campaign_lecturer_null_fk",
                        column: x => x.lecturerid,
                        principalTable: "user",
                        principalColumn: "id");
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "group",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    name = table.Column<string>(type: "text", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3"),
                    password = table.Column<string>(type: "longtext", nullable: false, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3"),
                    description = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3"),
                    zalolink = table.Column<string>(name: "zalo_link", type: "longtext", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3"),
                    lecturerid = table.Column<Guid>(name: "lecturer_id", type: "char(36)", nullable: false, collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id);
                    table.ForeignKey(
                        name: "fk_group_users_lecturer_id",
                        column: x => x.lecturerid,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "user_device_token",
                columns: table => new
                {
                    userid = table.Column<Guid>(name: "user_id", type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    devicetokenid = table.Column<Guid>(name: "device_token_id", type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    isloggedin = table.Column<bool>(name: "is_logged_in", type: "tinyint(1)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => new { x.userid, x.devicetokenid })
                        .Annotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                    table.ForeignKey(
                        name: "user_device_token_device_token_null_fk",
                        column: x => x.devicetokenid,
                        principalTable: "device_token",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "user_device_token_user_null_fk",
                        column: x => x.userid,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "wallet",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    userid = table.Column<Guid>(name: "user_id", type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    totalpoints = table.Column<uint>(name: "total_points", type: "int unsigned", nullable: false),
                    wallettype = table.Column<string>(name: "wallet_type", type: "text", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id);
                    table.ForeignKey(
                        name: "wallet_user_null_fk",
                        column: x => x.userid,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "campaign_department",
                columns: table => new
                {
                    campaignid = table.Column<Guid>(name: "campaign_id", type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    departmentid = table.Column<uint>(name: "department_id", type: "int unsigned", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => new { x.campaignid, x.departmentid })
                        .Annotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                    table.ForeignKey(
                        name: "campaign_department_campaign_null_fk",
                        column: x => x.campaignid,
                        principalTable: "campaign",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "campaign_department_department_null_fk",
                        column: x => x.departmentid,
                        principalTable: "department",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "scan",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    createdat = table.Column<DateTime>(name: "created_at", type: "datetime", nullable: false, defaultValueSql: "current_timestamp()"),
                    points = table.Column<uint>(type: "int unsigned", nullable: false),
                    campaignid = table.Column<Guid>(name: "campaign_id", type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    studentid = table.Column<Guid>(name: "student_id", type: "char(36)", nullable: false, collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id);
                    table.ForeignKey(
                        name: "scan_campaign_null_fk",
                        column: x => x.campaignid,
                        principalTable: "campaign",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "scan_student_null_fk",
                        column: x => x.studentid,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "campaign_group",
                columns: table => new
                {
                    campaignid = table.Column<Guid>(name: "campaign_id", type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    groupid = table.Column<Guid>(name: "group_id", type: "char(36)", nullable: false, collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => new { x.campaignid, x.groupid })
                        .Annotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                    table.ForeignKey(
                        name: "campaign_group_campaign_null_fk",
                        column: x => x.campaignid,
                        principalTable: "campaign",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "campaign_group_group_null_fk",
                        column: x => x.groupid,
                        principalTable: "group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "group_student",
                columns: table => new
                {
                    groupid = table.Column<Guid>(name: "group_id", type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    studentid = table.Column<Guid>(name: "student_id", type: "char(36)", nullable: false, collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => new { x.groupid, x.studentid })
                        .Annotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                    table.ForeignKey(
                        name: "group_student_group_null_fk",
                        column: x => x.groupid,
                        principalTable: "group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "group_student_student_null_fk",
                        column: x => x.studentid,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "message",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    content = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3"),
                    groupid = table.Column<Guid>(name: "group_id", type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    createddatetime = table.Column<DateTime>(name: "created_date_time", type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_message", x => x.id);
                    table.ForeignKey(
                        name: "fk_message_groups_group_id",
                        column: x => x.groupid,
                        principalTable: "group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateTable(
                name: "transaction",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    walletid = table.Column<Guid>(name: "wallet_id", type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    voucherid = table.Column<Guid>(name: "voucher_id", type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    description = table.Column<string>(type: "longtext", nullable: false, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3"),
                    points = table.Column<uint>(type: "int unsigned", nullable: false),
                    createddatetime = table.Column<DateTime>(name: "created_datetime", type: "datetime", nullable: false),
                    item = table.Column<string>(type: "longtext", nullable: false, collation: "utf8mb3_general_ci")
                        .Annotation("MySql:CharSet", "utf8mb3")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id);
                    table.ForeignKey(
                        name: "transaction_voucher_null_fk",
                        column: x => x.voucherid,
                        principalTable: "voucher",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "transaction_wallet_null_fk",
                        column: x => x.walletid,
                        principalTable: "wallet",
                        principalColumn: "id");
                })
                .Annotation("MySql:CharSet", "utf8mb3")
                .Annotation("Relational:Collation", "utf8mb3_general_ci");

            migrationBuilder.CreateIndex(
                name: "ix_campaign_lecturer_id",
                table: "campaign",
                column: "lecturer_id");

            migrationBuilder.CreateIndex(
                name: "ix_campaign_department_department_id",
                table: "campaign_department",
                column: "department_id");

            migrationBuilder.CreateIndex(
                name: "ix_campaign_group_group_id",
                table: "campaign_group",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "ix_department_university_department_id",
                table: "department_university",
                column: "department_id");

            migrationBuilder.CreateIndex(
                name: "ix_department_university_university_id",
                table: "department_university",
                column: "university_id");

            migrationBuilder.CreateIndex(
                name: "ix_group_lecturer_id",
                table: "group",
                column: "lecturer_id");

            migrationBuilder.CreateIndex(
                name: "ix_group_name",
                table: "group",
                column: "name")
                .Annotation("MySql:IndexPrefixLength", new[] { 10 });

            migrationBuilder.CreateIndex(
                name: "ix_group_student_group_id",
                table: "group_student",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "ix_group_student_student_id",
                table: "group_student",
                column: "student_id");

            migrationBuilder.CreateIndex(
                name: "ix_message_group_id",
                table: "message",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "ix_organization_name",
                table: "organization",
                column: "name");

            migrationBuilder.CreateIndex(
                name: "ix_scan_campaign_id",
                table: "scan",
                column: "campaign_id");

            migrationBuilder.CreateIndex(
                name: "ix_scan_student_id",
                table: "scan",
                column: "student_id");

            migrationBuilder.CreateIndex(
                name: "ix_transaction_voucher_id",
                table: "transaction",
                column: "voucher_id");

            migrationBuilder.CreateIndex(
                name: "ix_transaction_wallet_id",
                table: "transaction",
                column: "wallet_id");

            migrationBuilder.CreateIndex(
                name: "ix_user_department_id",
                table: "user",
                column: "department_id");

            migrationBuilder.CreateIndex(
                name: "ix_user_email",
                table: "user",
                column: "email")
                .Annotation("MySql:IndexPrefixLength", new[] { 20 });

            migrationBuilder.CreateIndex(
                name: "ix_user_university_id",
                table: "user",
                column: "university_id");

            migrationBuilder.CreateIndex(
                name: "ix_user_username",
                table: "user",
                column: "username")
                .Annotation("MySql:IndexPrefixLength", new[] { 10 });

            migrationBuilder.CreateIndex(
                name: "ix_user_device_token_device_token_id",
                table: "user_device_token",
                column: "device_token_id");

            migrationBuilder.CreateIndex(
                name: "ix_voucher_category_id",
                table: "voucher",
                column: "category_id");

            migrationBuilder.CreateIndex(
                name: "ix_voucher_created_date",
                table: "voucher",
                column: "created_date");

            migrationBuilder.CreateIndex(
                name: "ix_voucher_organization_id",
                table: "voucher",
                column: "organization_id");

            migrationBuilder.CreateIndex(
                name: "ix_wallet_user_id",
                table: "wallet",
                column: "user_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "campaign_department");

            migrationBuilder.DropTable(
                name: "campaign_group");

            migrationBuilder.DropTable(
                name: "department_university");

            migrationBuilder.DropTable(
                name: "group_student");

            migrationBuilder.DropTable(
                name: "message");

            migrationBuilder.DropTable(
                name: "scan");

            migrationBuilder.DropTable(
                name: "transaction");

            migrationBuilder.DropTable(
                name: "user_device_token");

            migrationBuilder.DropTable(
                name: "group");

            migrationBuilder.DropTable(
                name: "campaign");

            migrationBuilder.DropTable(
                name: "voucher");

            migrationBuilder.DropTable(
                name: "wallet");

            migrationBuilder.DropTable(
                name: "device_token");

            migrationBuilder.DropTable(
                name: "category");

            migrationBuilder.DropTable(
                name: "organization");

            migrationBuilder.DropTable(
                name: "user");

            migrationBuilder.DropTable(
                name: "department");

            migrationBuilder.DropTable(
                name: "university");
        }
    }
}
