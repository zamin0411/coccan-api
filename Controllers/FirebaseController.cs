﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace coccan_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FirebaseController : ControllerBase
    {

        // GET: api/<FirebaseController>
        [Authorize(Roles = "ADMIN")]
        [HttpGet]
        public ActionResult<string> Get() {
            return Ok(GetApiKey());
    }

    private string? GetApiKey()
        {
            IConfiguration config = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", true, true)
            .Build();

            return config["Firebase:APIKey"];
        }
    } 
}
        
   