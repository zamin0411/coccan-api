﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using coccan_api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace coccan_api.Controllers
{
    [Route("api/group-students")]
    //[Authorize]
    [ApiController]
    public class GroupStudentsController : ControllerBase
    {
        private readonly CoccanContext _context;
        private readonly IMapper _mapper;

        public GroupStudentsController(CoccanContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/GroupStudents
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GroupStudent>>> GetGroupStudent()
        {
            return await _context.GroupStudent.ToListAsync();
        }

        // GET: api/GroupStudents/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GroupStudent>> GetGroupStudent(Guid id)
        {
            var groupStudent = await _context.GroupStudent.FindAsync(id);

            if (groupStudent == null)
            {
                return NotFound();
            }

            return groupStudent;
        }

        // PUT: api/GroupStudents/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGroupStudent(Guid id, GroupStudent groupStudent)
        {
            if (id != groupStudent.GroupId)
            {
                return BadRequest();
            }

            _context.Entry(groupStudent).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!GroupStudentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);
                }
            }

            return NoContent();
        }

        // POST: api/GroupStudents
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<GroupStudent>> PostGroupStudent(PostGroupStudentDTO groupStudentDTO)
        {
            var groupStudent = _mapper.Map<GroupStudent>(groupStudentDTO);
            _context.GroupStudent.Add(groupStudent);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                if (GroupStudentExists(groupStudent.GroupId))
                {
                    return Conflict();
                }
                else
                {
                    return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);
                }
            }

            return CreatedAtAction("GetGroupStudent", new { id = groupStudent.GroupId }, groupStudent);
        }

        // DELETE: api/GroupStudents/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGroupStudent(Guid id)
        {
            var groupStudent = await _context.GroupStudent.FindAsync(id);
            if (groupStudent == null)
            {
                return NotFound();
            }

            _context.GroupStudent.Remove(groupStudent);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool GroupStudentExists(Guid id)
        {
            return _context.GroupStudent.Any(e => e.GroupId == id);
        }
    }
}
