﻿using AutoMapper;
using FirebaseAdmin.Messaging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static Google.Apis.Requests.BatchRequest;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace coccan_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NotificationsController : ControllerBase
    {
        private readonly Models.CoccanContext _context;

        public NotificationsController(Models.CoccanContext context)
        {
            _context = context;
        }


        // POST api/<Notifications>
        [HttpPost]
        public async Task<IActionResult> Post(Guid userId, int points)
        {
            
            if (!_context.Users.Any(u => u.Id == userId))
            {
                return NotFound();
            }
            else
            {
                var userDeviceTokens = await _context.UserDeviceTokens.Where(u => u.UserId.Equals(userId)).Include(u => u.DeviceToken).ToListAsync();
                var registrationTokens = new List<string>();

                // Add token to list if logged in
                foreach (var item in userDeviceTokens)
                    if (item.IsLoggedIn)
                        registrationTokens.Add(item.DeviceToken.Code);

                // Send messages
                if (registrationTokens.Count > 0)
                {


                    var multicastMessage = new MulticastMessage()
                    {
                        Notification = new Notification()
                        {
                            Title = "Admin cho diem",
                            Body = points.ToString()
                        },
                        Tokens = registrationTokens,
                        Data = new Dictionary<string, string>()
                    {
                        { "points", points.ToString() },
                        { "time", DateTime.Now.ToString() },
                    },
                        Android = new AndroidConfig
                        {
                            Priority = Priority.High
                        }

                    };


                    var response = await FirebaseMessaging.DefaultInstance.SendMulticastAsync(multicastMessage);

                    //var message = new Message()
                    //{
                    //    Notification = new Notification()
                    //    {
                    //        Title = "Admin cho diem",
                    //        Body = points.ToString()
                    //    },
                    //    Token = registrationTokens[0],
                    //    Data = new Dictionary<string, string>()
                    //    {
                    //        { "points", points.ToString() },
                    //        { "time", DateTime.Now.ToString() },
                    //    },
                    //    Android = new AndroidConfig
                    //    {
                    //        Priority = Priority.High
                    //    }
                    //};

                    //var response = await FirebaseMessaging.DefaultInstance.SendAsync(message);
                    return Ok(response);
                }
                else {
                    return BadRequest();
                }
            }
            
        }
        

    }
}
