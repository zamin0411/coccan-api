﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using coccan_api.Models;
using Microsoft.AspNetCore.Authorization;
using coccan_api.Metadata;
using coccan_api.Services;


namespace coccan_api.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizationsController : ControllerBase
    {
        private readonly CoccanContext _context;
        private readonly ICloudStorageService _cloudStorageService;

        public OrganizationsController(CoccanContext context, ICloudStorageService cloudStorageService)
        {
            _context = context;
            _cloudStorageService = cloudStorageService;
        }

        // GET: api/Organizations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Organization>>> GetOrganizations()
        {
            return await _context.Organizations.ToListAsync();
        }

        // GET: api/Organizations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Organization>> GetOrganization(ulong id)
        {
            var organization = await _context.Organizations.FindAsync(id);

            if (organization == null)
            {
                return NotFound();
            }

            return organization;
        }

        // PUT: api/Organizations/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrganization(ulong id, [FromForm] PutOrganizationDTO organizationDTO)
        {
            var uuid = Guid.NewGuid();
            var file = organizationDTO.Logo;
            var name = organizationDTO.Name;
            string logo = organizationDTO.LogoLink;
            if (file != null)
                logo = await _cloudStorageService.UploadFileAsync(file, "logo/" + name + uuid);
            var description = organizationDTO.Description;
            var organization = new Organization
            {
                Id = id,
                Name = name,
                Logo = logo,
                Description = description,
            };
            _context.Entry(organization).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!OrganizationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);
                }
            }

            return Ok();
        }

        // POST: api/Organizations
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Organization>> PostOrganization([FromForm]PostOrganizationDTO postOrganizationDTO)
        {
            var uuid = Guid.NewGuid();
            var file = postOrganizationDTO.Logo;
            var name = postOrganizationDTO.Name;
            var logo = await _cloudStorageService.UploadFileAsync(file, "logo/" + name + uuid);
            var organization = new Organization
            {
                Name = name,
                Logo = logo,
                Description = postOrganizationDTO.Description,
            };
            _context.Organizations.Add(organization);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetOrganization", new { id = organization.Id }, organization);
        }

        // DELETE: api/Organizations/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrganization(ulong id)
        {
            var organization = await _context.Organizations.FindAsync(id);
            if (organization == null)
            {
                return NotFound();
            }

            _context.Organizations.Remove(organization);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool OrganizationExists(ulong id)
        {
            return _context.Organizations.Any(e => e.Id == id);
        }
    }
}
