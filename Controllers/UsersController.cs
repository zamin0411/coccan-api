﻿using AutoMapper;
using coccan_api.Metadata;
using coccan_api.Models;
using coccan_api.Repositories;
using coccan_api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using System.Security.Claims;
using System.Text.Json;

namespace coccan_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly CoccanContext _context;
        private readonly IMapper _mapper;
        private readonly IUserRepository _repo;
        private readonly IUserService _service;

        public UsersController(CoccanContext context, IMapper mapper, IUserRepository repo, IUserService service)
        {
            _context = context;
            _mapper = mapper;
            _repo = repo;
            _service = service;

        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers(int limit, string? lastName, string? name)
        {
            var totalPage = await _context.Users.CountAsync();
            if (lastName != null)
            {
                var nextPage = await _context.Users
                                .OrderBy(u => u.Username)
                                .Where(u => u.Username.CompareTo(lastName) > 0)
                                .Take(limit)
                                .ToListAsync();
            } else
            {

            }

            var users = await _repo.GetAsync();
            IEnumerable<UserResponseDTO> results = _mapper.Map<IEnumerable<User>, IEnumerable <UserResponseDTO>>(users);
            return Ok(results);
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(Guid id)
        {
            

            var user = await _context.Users.Where(u => u.Id == id).Include(u => u.Wallets).Include(u => u.Department).Include(u => u.University).SingleOrDefaultAsync();

            if (user == null)
            {
                return NotFound();
            }
            var result = _mapper.Map<UserResponseDTO>(user);

            return Ok(result); 
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(Guid id, PostUserDTO userDTO)
        {
            var user = _mapper.Map<User>(userDTO);
            user.Id = id;
            _context.Users.Update(user);

            try
            {

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);
                }
            }

            return Ok();
        }

        //POST: api/Users
        //To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<User>> PostUser([FromBody] PostUserDTO userDTO)
        {
            var user = _mapper.Map<PostUserDTO, User>(userDTO);
            var id = await _service.CreateNewUser(user);
            user = await _context.Users.Where(u => u.Id == id).Include(u => u.Wallets).Include(u => u.Department).Include(u => u.University).SingleOrDefaultAsync();

            if (user == null)
            {
                return Problem(detail: "Fail creating new user", statusCode: 500);
            }

            return CreatedAtAction("GetUser", new { id = user.Id }, user);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> DeleteUser(Guid id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return Ok();
        }


        // PATCH: api/Users/08db0abe-5d9e-49fb-896a-b85f2d1c9059
        [HttpPatch("{id}")]
        public async Task<ActionResult<User>> PatchUser(Guid id,
    [FromBody] JsonPatchDocument<User> patchDoc)
        {
            var user = await _context.Users.FindAsync(id);
            if (user != null) { 
                //{
                //    foreach (var field in typeof(PatchUserDTO).GetFields(BindingFlags.Instance |
                //                                     BindingFlags.NonPublic |
                //                                     BindingFlags.Public))
                //    {
                //        if (field.GetValue(userDTO) == null)
                //        {

                //        }
                //    }

                try
            {
                patchDoc.ApplyTo(user);
            }
            catch (JsonPatchException)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {

                    return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);

                }
            return Ok(user);
        }
            else
            {
                return NotFound();
            }

        }

        [HttpGet("{id}/groups")]
        [Authorize(Roles = "LECTURER, STUDENT")]
        public async Task<ActionResult<IEnumerable<GroupResponseDTO>>> GetGroupsByUserId(Guid id)
        {
            var headers = HttpContext.Request.Headers;
            var jwt = headers["Authorization"].ToString().Replace("Bearer", string.Empty).TrimStart();
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(jwt);
            var role = token.Claims.ElementAt(0).Value;
            List<Group> groups = new List<Group>();
            if (role.ToUpper() == Role.STUDENT.ToString())
            {
                groups = await _context.Groups.Include(g => g.GroupStudents).ThenInclude(gs => gs.Student).Where(g => g.GroupStudents.Any(gs => gs.StudentId == id)).Include(g => g.Lecturer.University).Include(g => g.Lecturer.Department).Include(g => g.Lecturer.Wallets).Include(g => g.Messages).ToListAsync();
            }
            else if (role.ToUpper() == Role.LECTURER.ToString())
            {
                groups = await _context.Groups.Where(g => g.LecturerId == id).Include(g => g.Lecturer.University).Include(g => g.Lecturer.Department).Include(g => g.Lecturer.Wallets).Include(g => g.GroupStudents).ThenInclude(gs => gs.Student).Include(g => g.Messages).ToListAsync();
            }
            List<GroupResponseDTO> results = _mapper.Map<List<Group>, List<GroupResponseDTO>>(groups);

            return Ok(results);
        }

        [HttpGet("{id}/campaigns")]
        [Authorize(Roles = "LECTURER, STUDENT")]
        public async Task<ActionResult<IEnumerable<CampaignResponseDTO>>> GetCampaignsByUserId(Guid id)
        {
            var headers = HttpContext.Request.Headers;
            var jwt = headers["Authorization"].ToString().Replace("Bearer", string.Empty).TrimStart();
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(jwt);
            var role = token.Claims.ElementAt(0).Value;
            List<Campaign> campaigns = new List<Campaign>();
            if (role.ToUpper() == Role.STUDENT.ToString())
            {
                var user = _context.Users.Where(u => u.Id == id);


                // All

                campaigns.AddRange(await _context.Campaigns.Where(c => c.Target == CampaignTarget.ALL.ToString() && c.ExpiredDate.CompareTo(DateTime.Now) >= 0).Include(c => c.Lecturer.University).Include(c => c.Scans).ToListAsync());
                // Department
                uint departmentId = await user.Select(u => u.DepartmentId).SingleOrDefaultAsync();
                campaigns.AddRange(await _context.Campaigns.Include(c => c.Departments).Where(c => c.Departments.Any(d => d.Id == departmentId) && c.ExpiredDate.CompareTo(DateTime.Now) >= 0).Include(c => c.Lecturer.University).Include(c => c.Scans).ThenInclude(s => s.Student).ToListAsync());

                // Group
                var groupsId = await user.Include(u => u.GroupStudents).Select(u => u.GroupStudents.Select(g => g.GroupId)).SingleOrDefaultAsync();
                if (groupsId != null)
                    if (groupsId.Any())
                        campaigns.AddRange(await _context.Campaigns.Include(c => c.Groups).Where(c => (c.Groups.Any(g => groupsId.Any(id => id == g.Id)) && c.ExpiredDate.CompareTo(DateTime.Now) >= 0)).ToListAsync());
            }
            else if (role.ToUpper() == Role.LECTURER.ToString())
            {
                campaigns = await _context.Campaigns.Where(g => g.LecturerId == id).Include(c=> c.Lecturer.University).Include(c => c.Departments).Include(c => c.Groups).Include(c => c.Scans).ThenInclude(s => s.Student).ToListAsync();
            }
            List<CampaignResponseDTO> results = _mapper.Map<List<Campaign>, List<CampaignResponseDTO>>(campaigns);

            return Ok(results);
        }

        [HttpGet("{id}/scans")]
        [Authorize(Roles = "STUDENT")]
        public async Task<ActionResult<IEnumerable<ScanResponseDTO>>> GetScannedCampaignsByUserId(Guid id)
        {

            var scans = await _context.Scans.Where(s => s.StudentId == id).Include(s => s.Campaign.Lecturer.Department).Include(s => s.Campaign.Lecturer.University).ToListAsync();
            List<ScanResponseDTO> results = _mapper.Map<List<Scan>, List<ScanResponseDTO>>(scans);

            return Ok(results);
        }


        [HttpGet("{id}/vouchers")]
        //[Authorize(Roles = "STUDENT, LECTURER")]
        public async Task<ActionResult<IEnumerable<UserVoucherResponseDTO>>> GetVouchersByUserId(Guid id)
        {
            var transactions = await _context.Transactions.Include(t => t.Wallet).Include(vu => vu.Voucher.Organization).Include(vu => vu.Voucher.Category).Where(t => t.VoucherId != null && t.Wallet.UserId == id ).ToListAsync();
            var results = _mapper.Map<List<Transaction>, List<UserVoucherResponseDTO>>(transactions);
            return Ok(results);
        }


        private bool UserExists(Guid id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
    }
}
