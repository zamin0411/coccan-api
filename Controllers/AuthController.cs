﻿using AutoMapper;
using coccan_api.Metadata;
using coccan_api.Models;
using FirebaseAdmin.Auth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using coccan_api.Services;

namespace coccan_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly CoccanContext _context;
        
        public AuthController(CoccanContext context)
        {
            _context = context;
        }
        //POST: api/auth
        //To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<AuthResponseDTO>> Login([FromBody(EmptyBodyBehavior = EmptyBodyBehavior.Allow)] LoginBody? loginBody = null)
        {
            var headers = HttpContext.Request.Headers;
             var idToken = headers["authentication"];

            if (idToken.Count == 0 || idToken.Count > 1) { return Unauthorized(); }
            FirebaseToken decodedToken;
            try
            {
                // Verify idToken
                decodedToken = await FirebaseAuth.DefaultInstance
    .VerifyIdTokenAsync(idToken);
            }
            catch (Exception ex)
            {
                return Problem(detail: ex.Message, statusCode: 401);
            }


            var email = (string)decodedToken.Claims["email"];
            var username = (string)decodedToken.Claims["name"];
            var profilePhoto = (string)decodedToken.Claims["picture"];
            var registrationToken = loginBody?.RegistrationToken;

            // For debugging
            //var email = addUserDTO.Email;
            //var profilePhoto = addUserDTO.ProfilePhoto;
            //var username = addUserDTO.Username;
            //var registrationToken = addUserDTO.RegistrationToken;

            // Add user if new
            var user = await _context.Users.Where(u => u.Email == email).Include(u => u.Wallets).Include(u => u.University).Include(u => u.Department).Include(u => u.UserDeviceTokens).ThenInclude(udt => udt.DeviceToken).SingleOrDefaultAsync();
            UserDeviceToken userDeviceToken;
            if (user == null)
            {

                user = new User { Email = email, ProfilePhoto = profilePhoto, Username = username };
                Wallet wallet = new Wallet
                {
                    TotalPoints = 0,
                    WalletType = WalletType.TOEXCHANGE.ToString()

                };
                user.Wallets.Add(wallet);

                // Logged in on mobiles
                if (registrationToken != null)
                {
                    var deviceToken = new DeviceToken { Code = registrationToken};
                    try
                    {
                        userDeviceToken = new UserDeviceToken { User = user, DeviceToken = deviceToken };
                        _context.UserDeviceTokens.Add(userDeviceToken);
                        await _context.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        return Problem(detail: ex.Message, statusCode: 500);
                    }

                }
                else

                // Web
                {
                    try
                    {
                        _context.Users.Add(user);
                        await _context.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        return Problem(detail: ex.Message, statusCode: 500);

                    }
                }
            }

            // User exists
            else
            {
                // Check if user logged in with new mobile device
                if (registrationToken != null)
                {
                    int index = user.UserDeviceTokens.Contains(registrationToken);
                    if (index == -1)
                    {
                        var deviceToken = await _context.DeviceTokens.Where(t => t.Code == registrationToken).SingleOrDefaultAsync();
                        if (deviceToken == null)
                        {
                            deviceToken = new DeviceToken { Code = registrationToken };
                        }
                        userDeviceToken = new UserDeviceToken { User = user, DeviceToken = deviceToken };
                        _context.UserDeviceTokens.Add(userDeviceToken);
                        try
                        {
                            await _context.SaveChangesAsync();
                        }
                        catch (Exception ex)
                        {
                            return Problem(detail: ex.Message, statusCode: 500);
                        }
                    }
                }

            }
            var role = user.Role;
            user = await _context.Users.Where(u => u.Email == email).Include(u => u.Wallets).Include(u => u.Department).Include(u => u.University).SingleOrDefaultAsync();

            // Create JWT
            IConfiguration config = new ConfigurationBuilder()
     .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", true, true)
    .Build();

            // Json serialize object
            DefaultContractResolver contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy()
            };

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                ContractResolver = contractResolver,
                Formatting = Formatting.Indented
            };

            string wallets = JsonConvert.SerializeObject(user.Wallets, settings);
            string university = JsonConvert.SerializeObject(user.University, settings);
            string department = JsonConvert.SerializeObject(user.Department, settings);

            var issuer = config["Jwt:Issuer"];
            var audience = config["Jwt:Audience"];
            var key = Encoding.ASCII.GetBytes(config["Jwt:Key"]);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
        {
                        new Claim(ClaimTypes.Role, role),
                new Claim("id", user.Id.ToString()),
                new Claim("username", username),
                new Claim(JwtRegisteredClaimNames.Email, email),
                new Claim("university", university, JsonClaimValueTypes.Json),
                new Claim("department", department, JsonClaimValueTypes.Json),
                new Claim("profilePhoto", profilePhoto),
                new Claim("wallets", wallets, JsonClaimValueTypes.JsonArray),
                new Claim(JwtRegisteredClaimNames.Jti,
                Guid.NewGuid().ToString())
             }),
                Expires = DateTime.UtcNow.AddDays(30),
                Issuer = issuer,
                Audience = audience,
                SigningCredentials = new SigningCredentials
        (new SymmetricSecurityKey(key),
        SecurityAlgorithms.HmacSha512Signature)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var jwtToken = tokenHandler.WriteToken(token);
            var stringToken = tokenHandler.WriteToken(token);

            return Ok(new AuthResponseDTO { Token = stringToken });

         }
    }
}
