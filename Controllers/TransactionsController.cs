﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using coccan_api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace coccan_api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]



    
    public class TransactionsController : ControllerBase
    {
        private readonly CoccanContext _context;
        private readonly IMapper _mapper;
        static Random random = new Random();


        public TransactionsController(CoccanContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Transactions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Transaction>>> GetTransactions()
        {
            return await _context.Transactions.ToListAsync();
        }

        // GET: api/Transactions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Transaction>> GetTransaction(Guid id)
        {
            var transaction = await _context.Transactions.FindAsync(id);

            if (transaction == null)
            {
                return NotFound();
            }

            return transaction;
        }

        // PUT: api/Transactions/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTransaction(Guid id, Transaction transaction)
        {
            if (id != transaction.Id)
            {
                return BadRequest();
            }

            _context.Entry(transaction).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransactionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        // POST: api/Transactions
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Transaction>> PostTransaction(PostTransactionDTO transactionDTO)
        {
            var transaction = _mapper.Map<Transaction>(transactionDTO);
            if (_context.Transactions.Any(e => e.VoucherId == transaction.VoucherId && e.WalletId == transaction.WalletId))
                return Problem(title: "Forbidden", detail: "Dc doi 1 lan thui ma oi", statusCode: 403);
            try {

                var wallet = await _context.Wallets.FindAsync(transactionDTO.WalletId);

                if (wallet != null)
                {
                    int after = (int)wallet.TotalPoints + transaction.Points;
                    wallet.TotalPoints = (uint)after;
                    _context.Entry(wallet).State = EntityState.Modified;
                }
                else
                    return BadRequest();
                var voucher = await _context.Vouchers.FindAsync(transaction.VoucherId);

                if (voucher == null) 
                    return NotFound();
                if (voucher.Number == 0) 
                    return NotFound();

                voucher.Number -= 1;  
                _context.Entry(voucher).State = EntityState.Modified;

                int lengthOfVoucher = 10;
                List<string> generatedVouchers = new List<string>();
                char[] keys = "ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890".ToCharArray();
                transaction.Item = GenerateVoucher(keys, lengthOfVoucher);
                
                _context.Transactions.Add(transaction);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Problem(detail: ex.Message, statusCode: 500);
            }
            

            return CreatedAtAction("GetTransaction", new { id = transaction.Id }, transaction);
        }

        // DELETE: api/Transactions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTransaction(Guid id)
        {
            var transaction = await _context.Transactions.FindAsync(id);
            if (transaction == null)
            {
                return NotFound();
            }

            _context.Transactions.Remove(transaction);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool TransactionExists(Guid id)
        {
            return _context.Transactions.Any(e => e.Id == id);
        }

        private string GenerateVoucher(char[] keys, int lengthOfVoucher)
        {
            return Enumerable.Range(1, lengthOfVoucher) // for(i.. ) 
                                        .Select(k => keys[random.Next(0, keys.Length - 1)])  // generate a new random char 
                                        .Aggregate("", (e, c) => e + c); // join into a string
        }
    }
}
