﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using coccan_api.Models;
using AutoMapper;
using coccan_api.Metadata;

namespace coccan_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScansController : ControllerBase
    {
        private readonly CoccanContext _context;
        private readonly IMapper _mapper;

        public ScansController(CoccanContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Scans
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Scan>>> GetScans()
        {
            return await _context.Scans.ToListAsync();
        }

        // GET: api/Scans/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Scan>> GetScan(Guid id)
        {
            var scan = await _context.Scans.FindAsync(id);

            if (scan == null)
            {
                return NotFound();
            }

            return scan;
        }

        // PUT: api/Scans/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutScan(Guid id, Scan scan)
        {
            if (id != scan.Id)
            {
                return BadRequest();
            }

            _context.Entry(scan).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex )
            {
                if (!ScanExists(id))
                {
                    return NotFound();
                }
                else
                {
                    return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);
                }
            }

            return NoContent();
        }

        // POST: api/Scans
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Scan>> PostScan(PostScanDTO scanDTO)
        {
            var campaignId = scanDTO.CampaignId;
            var studentId = scanDTO.StudentId;
            var campaignExists = _context.Campaigns.Any(c => c.Id == campaignId);
            // Check error
            if (!campaignExists) return Problem(title: "Bad Request", statusCode: 400, detail: "Campaign does not exist!");
            var campaign = await _context.Campaigns.Include(c => c.Scans).Where(c => c.Id == campaignId).SingleOrDefaultAsync();
            if (campaign.Scans.Count() > campaign.ParticipantsLimit) return Problem(title: "Bad Request", statusCode: 400, detail: "Du roi!");
            if (campaign.ExpiredDate.CompareTo(DateTime.Now) < 0) return Problem(title: "Bad Request", statusCode: 400, detail: "Het han!");
            if (campaign.Scans.Any(s => s.StudentId == studentId)) return Problem(title: "Bad Request", statusCode: 400, detail: "Tham lam!");


            var wallets = await _context.Users.Include(u => u.Wallets).Where(u => u.Id == studentId).Select(u => u.Wallets).SingleOrDefaultAsync();
            var wallet = wallets.Where(w => w.WalletType == WalletType.TOEXCHANGE.ToString()).FirstOrDefault();
            wallet.TotalPoints += campaign.PointsGiven / campaign.ParticipantsLimit;
            _context.Entry(wallet).State= EntityState.Modified;
            var scan = _mapper.Map<Scan>(scanDTO);
            scan.CreatedAt = DateTime.Now;
            scan.Points = campaign.PointsGiven / campaign.ParticipantsLimit;
            _context.Scans.Add(scan);
            await _context.SaveChangesAsync();
            _context.Entry(scan).Reference(s => s.Student).Query().Include(s => s.University).Include(s => s.Department).Load();

            return CreatedAtAction("GetScan", new { id = scan.Id }, scan);
        }

        // DELETE: api/Scans/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteScan(Guid id)
        {
            var scan = await _context.Scans.FindAsync(id);
            if (scan == null)
            {
                return NotFound();
            }

            _context.Scans.Remove(scan);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ScanExists(Guid id)
        {
            return _context.Scans.Any(e => e.Id == id);
        }
    }
}
