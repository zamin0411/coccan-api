﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using coccan_api.Models;
using StackExchange.Redis;
using coccan_api.Redis;
using AutoMapper;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using coccan_api.Metadata;
using Hangfire;
using coccan_api.Services;

namespace coccan_api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class VouchersController : ControllerBase
    {
        private readonly CoccanContext _context;
        private readonly RedisConnection _redis;
        private readonly IMapper _mapper;
        private readonly IVoucherService _voucherService;

        public VouchersController(CoccanContext context, RedisConnection redis, IMapper mapper, IVoucherService voucherService)
        {
            _context = context;
            _redis = redis;
            _mapper = mapper;
            _voucherService = voucherService;
        }

        // GET: api/Vouchers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Voucher>>> GetVouchers(DateTime? after, int size = 10)
        {

            try
            {
                var key = $"vouchers-{after}-{size}";
                var cachedVouchers = await _redis.BasicRetryAsync(async (db) => await db.StringGetAsync(key));
                if (cachedVouchers.IsNull)
                {
                    var vouchers = await _voucherService.GetAsync(after, size);
                    _ = BackgroundJob.Enqueue(() => _voucherService.BackgroundCacheVouchers(key, vouchers));
                    //await _voucherService.BackgroundCacheVouchers(vouchers);
                    return Ok(vouchers);
                }
                else
                {
                    var result = JsonConvert.DeserializeObject<List<Voucher>>(cachedVouchers);
                    return Ok(result);
                }
            } catch (RedisTimeoutException ex) {
                return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);

            }


        }

        // GET: api/Vouchers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Voucher>> GetVoucher(Guid id)
        {
            try
            {
                var cachedVoucher = await _redis.BasicRetryAsync(async (db) => await db.StringGetAsync("voucher " + id.ToString()));
                if (cachedVoucher.IsNull)
                {
                    var voucher = await _context.Vouchers.Where(v => v.Id == id).Include(v => v.Organization).Include(v => v.Category).SingleOrDefaultAsync();
                    if (voucher != null)
                    {
                        _ = BackgroundJob.Enqueue(() => _voucherService.BackgroundCacheVoucher(voucher));
                        return Ok(voucher);
                    }
                    else
                        return NotFound();

                }
                else
                    return Ok(cachedVoucher);
            } catch (RedisTimeoutException ex)
            {
                return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);

            }
        }

        // PUT: api/Vouchers/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<ActionResult<Voucher>> PutVoucher(Guid id, PostVoucherDTO voucherDTO)
        {
            var voucher = _mapper.Map<Voucher>(voucherDTO);
            voucher.Id = id;
            _context.Entry(voucher).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!VoucherExists(id))
                {
                    return NotFound();
                }
                else
                {
                    return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);
                }
            }
            try
            {
                _ = BackgroundJob.Enqueue(() => _voucherService.BackgroundCacheVouchers());
            }
            catch (RedisTimeoutException ex)
            {
                return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);
            }
            return Ok();
        }

        // POST: api/Vouchers
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        //[Authorize(Roles = "LECTURER")]
        public async Task<ActionResult<Voucher>> PostVoucher(PostVoucherDTO voucherDTO)
        {
            var voucher = _mapper.Map<Voucher>(voucherDTO);
            voucher.CreatedDate = DateTime.Now;
            _context.Vouchers.Add(voucher);
            try
            {
                await _context.SaveChangesAsync();
            } catch (DbUpdateException ex) {
                return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);
            }
            try
            {
                _ = BackgroundJob.Enqueue(() => _voucherService.BackgroundCacheVouchers());
            }catch (RedisConnectionException ex)
            {
                return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);
            }
            return CreatedAtAction("GetVoucher", new { id = voucher.Id }, voucher);
        }

        // DELETE: api/Vouchers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVoucher(Guid id)
        {
            var voucher = await _context.Vouchers.FindAsync(id);
            if (voucher == null)
            {
                return NotFound();
            }

            _context.Vouchers.Remove(voucher);
            try
            {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException ex)
            {
                return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);
            }


            _ = BackgroundJob.Enqueue(() => _voucherService.BackgroundCacheVouchers());

            return Ok();
        }

        private bool VoucherExists(Guid id)
        {
            return _context.Vouchers.Any(e => e.Id == id);
        }
    }
}
