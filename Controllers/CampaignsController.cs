﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using coccan_api.Models;
using AutoMapper;
using coccan_api.Metadata;

namespace coccan_api.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CampaignsController : ControllerBase
    {
        private readonly CoccanContext _context;
        private readonly IMapper _mapper;
        public CampaignsController(CoccanContext context, IMapper mapper)
        {
            _context = context;
            _mapper= mapper;
        }

        // GET: api/Campaigns
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CampaignResponseDTO>>> GetCampaigns()
        {
            var campaigns = await _context.Campaigns.Include(c => c.Departments).Include(c => c.Groups).Include(c => c.Lecturer.University).Include(c => c.Scans).ThenInclude(s => s.Student.University).Include(c => c.Scans).ThenInclude(s => s.Student.Department).ToListAsync();
            var results = _mapper.Map<IEnumerable<CampaignResponseDTO>>(campaigns);
            return Ok(results);
        }

        // GET: api/Campaigns/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CampaignResponseDTO>> GetCampaign(Guid id)
        {
            var campaign = await _context.Campaigns.Where(c => c.Id == id).Include(c => c.Departments).Include(c => c.Groups).Include(c => c.Lecturer.University).Include(c => c.Lecturer.Department).Include(c => c.Scans).ThenInclude(s => s.Student.University).Include(c => c.Scans).ThenInclude(s => s.Student.Department).SingleOrDefaultAsync();
            var result = _mapper.Map<CampaignResponseDTO>(campaign);
            if (campaign == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        // PUT: api/Campaigns/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCampaign(Guid id, PostCampaignDTO campaignDTO)
        {
            if (!CampaignExists(id))
            {
                return NotFound();
            }
            var campaign = _mapper.Map<Campaign>(campaignDTO);
            campaign.Id = id;
            var test = _context.CampaignDepartment;
            
            if (_context.CampaignGroup.Any(cg =>(string)cg["CampaignId"] == id.ToString()))
            {
                var campaignGroups = await _context.CampaignGroup.Where(cg =>(string) cg["CampaignId"] == id.ToString()).ToListAsync();
               
                _context.CampaignGroup.RemoveRange(campaignGroups);
            }


            if (_context.CampaignDepartment.Any(cd => (string)cd["CampaignId"] == id.ToString()))
            {
                var campaignDepartments = await _context.CampaignDepartment.Where(cg => (string)cg["CampaignId"] == id.ToString()).ToListAsync();


                _context.CampaignDepartment.RemoveRange(campaignDepartments);
            }
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {        
                    return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);
                
            }
            //var campaignGroup = await _context.CampaignGroup.Where(cg => (string)cg["CampaignId"] == id.ToString()).ToListAsync();

                if (campaign.Target == CampaignTarget.GROUP.ToString())
                {
                    _context.Groups.AttachRange(campaignDTO.Groups);
                }
                else if (campaign.Target == CampaignTarget.DEPARTMENT.ToString())
                {
                    _context.Departments.AttachRange(campaignDTO.Departments);

                }

                _context.Entry(campaign).State = EntityState.Modified;


                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {

                    return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);

                }
            
            return Ok();
        }

        // POST: api/Campaigns
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Campaign>> PostCampaign(PostCampaignDTO campaignDTO)
        {
            var campaign = _mapper.Map<PostCampaignDTO, Campaign>(campaignDTO);
            if (campaign.Groups != null)
                _context.Groups.AttachRange(campaign.Groups);
            if (campaign.Departments != null)
                _context.Departments.AttachRange(campaign.Departments);

            _context.Campaigns.Add(campaign);
            //_context.Entry(campaign).State = EntityState.Detached;
            //if (campaign.Groups != null)
            //    foreach (var group in campaign.Groups) 
            //        _context.Entry(group).State = EntityState.Detached;
            //if (campaign.Departments != null)
            //    foreach (var department in campaign.Departments)
            //        _context.Entry(department).State = EntityState.Detached;
            //var tmp = await _context.Campaigns.Where(c => c.Id == campaign.Id).Include(c => c.Departments).Include(c => c.Groups).Include(c => c.Lecturer.University).SingleOrDefaultAsync();

            await _context.Entry(campaign).Reference(c => c.Lecturer).Query().Include(l => l.University).Include(l => l.Department).Include(l => l.Wallets).LoadAsync();
            //await _context.Entry(campaign).Collection(c => c.Groups).LoadAsync();
            //await _context.Entry(campaign).Collection(c => c.Departments).LoadAsync();
            var wallet = campaign.Lecturer.Wallets.ElementAt(1);
            wallet.TotalPoints -= campaign.PointsGiven;
            _context.Entry(wallet).State= EntityState.Modified;
            await _context.SaveChangesAsync();

            var result = _mapper.Map<CampaignResponseDTO>(campaign);

            return CreatedAtAction("GetCampaign", new { id = campaign.Id }, result);
        }

        // DELETE: api/Campaigns/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCampaign(Guid id)
        {
            var campaign = await _context.Campaigns.FindAsync(id);
            if (campaign == null)
            {
                return NotFound();
            }

            _context.Campaigns.Remove(campaign);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool CampaignExists(Guid id)
        {
            return _context.Campaigns.Any(e => e.Id == id);
        }
    }
}
