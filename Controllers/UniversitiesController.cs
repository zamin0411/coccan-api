﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using coccan_api.Models;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;

namespace coccan_api.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UniversitiesController : ControllerBase
    {
        private readonly CoccanContext _context;
        private readonly IMapper _mapper;
        public UniversitiesController(CoccanContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Universities
        [HttpGet]
        public async Task<ActionResult<IEnumerable<University>>> GetUniversities()
        {
            return await _context.Universities.ToListAsync();
        }

        // GET: api/Universities/5
        [HttpGet("{id}")]
        public async Task<ActionResult<University>> GetUniversity(uint id)
        {
            var university = await _context.Universities.FindAsync(id);

            if (university == null)
            {
                return NotFound();
            }

            return university;
        }

        // PUT: api/Universities/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUniversity(uint id, PostUniversityDTO universityDTO)
        {
            var university = _mapper.Map<University>(universityDTO);
            university.Id = id;
            if (university.Departments != null)
                _context.Departments.AttachRange(university.Departments);
            _context.Entry(university).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UniversityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        // POST: api/Universities
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<University>> PostUniversity(PostUniversityDTO universityDTO)
        {
            var university = _mapper.Map<University>(universityDTO);
            if (university.Departments != null)
                    _context.Departments.AttachRange(university.Departments);
            _context.Universities.Add(university);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                return Problem(detail: ex.Message, statusCode: 500);

            }

            return CreatedAtAction("GetUniversity", new { id = university.Id }, university);
        }

        // DELETE: api/Universities/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUniversity(uint id)
        {
            var university = await _context.Universities.FindAsync(id);
            if (university == null)
            {
                return NotFound();
            }

            _context.Universities.Remove(university);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpGet("{id}/departments")]
        public async Task<IActionResult> GetDepartmentsByUniversityId(uint id)
        {
            var departments = await _context.Departments.Include(d => d.Universities).Where(d => d.Universities.Any(u => u.Id == id)).Select(d => new {d.Id, d.Name}).ToListAsync();
            return Ok(departments);
        }


        private bool UniversityExists(uint id)
        {
            return _context.Universities.Any(e => e.Id == id);
        }
    }
}
