﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using coccan_api.Models;
using Microsoft.AspNetCore.JsonPatch.Exceptions;
using Microsoft.AspNetCore.JsonPatch;
using AutoMapper;
using Humanizer.Localisation;
using static StackExchange.Redis.Role;
using coccan_api.Repositories;
using Microsoft.AspNetCore.Authorization;

namespace coccan_api.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GroupsController : ControllerBase
    {
        private readonly CoccanContext _context;
        private readonly IMapper _mapper;
        private readonly IGroupRepository _repo;

        public GroupsController(CoccanContext context, IMapper mapper, IGroupRepository repo)
        {
            _context = context;
            _mapper = mapper;
            _repo = repo;
        }

        //// GET: api/Groups
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Group>>> GetGroups()
        //{
        //    var groups = await _context.Groups.Include(g => g.Lecturer).Include(g => g.Messages).Include(g => g.GroupStudents).ThenInclude(g => g.Student).ToListAsync();
        //    List<GroupResponseDTO> results = _mapper.Map<List<Group>, List<GroupResponseDTO>>(groups);
        //    return Ok(results);
        //}

        // GET: api/Groups/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Group>> GetGroup(Guid id)
        {
            var @group = await _context.Groups.Include(g => g.Lecturer).Include(g => g.Messages).Where(g => g.Id == id).Include(g => g.GroupStudents).SingleOrDefaultAsync();
            var result = _mapper.Map<GroupResponseDTO>(@group);
            if (@group == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        // GET: api/Groups/5
        /// <summary>
        /// Search by group name or lecturer name
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<Group>> GetGroups([FromQuery] string? search)
        {
            IEnumerable<Group>  groups = await _repo.GetAsync(search);
            IEnumerable<GroupResponseDTO> results = _mapper.Map<IEnumerable<Group>, IEnumerable<GroupResponseDTO>>(groups);
            return Ok(results);
        }

        // PUT: api/Groups/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        //[Authorize(Roles = "LECTURER")]
        public async Task<IActionResult> PutGroup(Guid id, PostGroupDTO groupDTO)
        {
            var @group = _mapper.Map<Group>(groupDTO);
            group.Id= id;
            _context.Entry(@group).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!GroupExists(id))
                {
                    return NotFound();
                }
                else
                {
                    return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);
                }
            }
            _context.Entry(@group).Reference(g => g.Lecturer).Query().Include(l => l.Department).Include(l => l.University).Include(l => l.Wallets).Load();
            var result = _mapper.Map<GroupResponseDTO>(@group);
            return Ok(result);
        }

        // POST: api/Groups
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        //[Authorize(Roles ="LECTURER")]
        public async Task<ActionResult<Group>> PostGroup(PostGroupDTO groupDTO)
        {
            var @group = _mapper.Map<Group>(groupDTO);
            _context.Groups.Add(@group);
            await _context.SaveChangesAsync();
            _context.Entry(@group).Reference(g => g.Lecturer).Query().Include(l => l.Department).Include(l => l.University).Include(l => l.Wallets).Load();
            var result = _mapper.Map<GroupResponseDTO>(@group); 

            return CreatedAtAction("GetGroup", new { id = result.Id }, result);
        }

        // DELETE: api/Groups/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGroup(Guid id)
        {
            var @group = await _context.Groups.Include(g => g.GroupStudents).Where(g => g.Id == id).SingleOrDefaultAsync();
            if (@group == null)
            {
                return NotFound();
            }

            _context.Groups.Remove(@group);
            await _context.SaveChangesAsync();

            return Ok();
        }

        // PATCH: api/groups/08db0abe-5d9e-49fb-896a-b85f2d1c9059
        [HttpPatch("{id}")]

        public async Task<IActionResult> PatchGroup(Guid id,
    [FromBody] JsonPatchDocument<Group> patchDoc)
        {
            var group = await _context.Groups.FindAsync(id);
            if (group != null)
            {
                try
                {
                    patchDoc.ApplyTo(group);
                }
                catch (JsonPatchException)
                {
                    return BadRequest();
                }

                _context.Entry(group).State = EntityState.Modified;
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {

                    return Problem(title: ex.Message, detail: ex.InnerException?.Message, statusCode: 500);

                }
                return NoContent();
            }
            else
            {
                return NotFound();
            }

        }

        private bool GroupExists(Guid id)
        {
            return _context.Groups.Any(e => e.Id == id);
        }
    }
}
