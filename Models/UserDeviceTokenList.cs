﻿namespace coccan_api.Models
{
    public class UserDeviceTokenList : List<UserDeviceToken>
    {
        public int Contains(string? token)
        {
            if (token == null) return -1;
            if (!this.Any()) return -1;
            for (int i = 0; i < Count; i++)
            { 
                if (this[i].DeviceToken?.Code == token)
                    return i;
            }
            return -1;
        }
    }
}
