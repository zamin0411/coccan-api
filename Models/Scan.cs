﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace coccan_api.Models;

public partial class Scan
{
    public Guid Id { get; set; }

    public DateTime CreatedAt { get; set; }

    public uint Points { get; set; }

    public Guid CampaignId { get; set; }

    public Guid? StudentId { get; set; }
    [JsonIgnore]
    public Campaign Campaign { get; set; }

    public User Student { get; set; }
}

public record PostScanDTO
{

    public Guid CampaignId { get; set; }

    public Guid StudentId { get; set; }
}

public record ScanResponseDTO
{
    public DateTime CreatedAt { get; set; }

    public uint Points { get; set; }

    public Guid CampaignId { get; set; }

    public Guid? StudentId { get; set; }

    public CampaignResponseDTO Campaign { get; set; }
}
