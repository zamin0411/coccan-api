﻿using coccan_api.Metadata;
using Newtonsoft.Json;

namespace coccan_api.Models;

public partial class Campaign
{
    public Guid Id { get; set; }

    public Guid LecturerId { get; set; }

    public uint PointsGiven { get; set; }

    public uint ParticipantsLimit { get; set; }

    public DateTime ExpiredDate { get; set; }

    public string? Target { get; set; }

    [JsonIgnore]
    public virtual User Lecturer { get; set; } = null!;

    public ICollection<Department> Departments { get; set; } = new List<Department>();

    public ICollection<Group> Groups { get; set; } = new List<Group>();

    public ICollection<Scan> Scans { get; set; } = new List<Scan>();
}

public record PostCampaignDTO
{
    public Guid LecturerId { get; set; }

    public uint PointsGiven { get; set; }

    public DateTime ExpiredDate { get; set; }

    public string Target { get; set; }
    public uint ParticipantsLimit { get; set; }

    //public List<uint>? Departments { get; set; } = null!;

    //public List<Guid>? Groups { get; set; } = null!;

    public ICollection<Department>? Departments { get; set; } = new List<Department>();

    public ICollection<Group>? Groups { get; set; } = new List<Group>();
}

public record CampaignResponseDTO
{
    public Guid Id { get; set; }

    public User Lecturer { get; set; }

    public uint PointsGiven { get; set; }

    public uint ParticipantsLimit { get; set; }

    public DateTime ExpiredDate { get; set; }

    public string Target { get; set; }

    public ICollection<Department> Departments { get; set; } = null!;

    public ICollection<Group> Groups { get; set; } = null!;

    public ICollection<Scan> Scans { get; set; } = null!;

}
