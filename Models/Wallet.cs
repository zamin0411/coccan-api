﻿using Newtonsoft.Json;

namespace coccan_api.Models;

public partial class Wallet
{
    public Guid Id { get; set; }

    public Guid UserId { get; set; }

    public uint TotalPoints { get; set; }

    public string? WalletType { get; set; }
    
    [JsonIgnore]
    public virtual User? User { get; set; } = null!;
    [JsonIgnore]
    public ICollection<Transaction>? Transactions { get; set; }
}
