﻿using System;
using System.Collections.Generic;

namespace coccan_api.Models;

public partial class Transaction
{
    public Guid Id { get; set; }

    public Guid WalletId { get; set; }

    public Guid? VoucherId { get; set;}

    public string Description { get; set; }

    public int Points { get; set; }

    public DateTime CreatedDatetime { get; set; }

    public string Item { get; set; }
    public Wallet Wallet { get; set; }
    public Voucher Voucher { get; set; } = null!;
}

public record PostTransactionDTO
{
    public Guid WalletId { get; set; }

    public Guid? VoucherId { get; set; }

    public string Description { get; set; }

    public int Points { get; set; }

}



