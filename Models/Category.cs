﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace coccan_api.Models;

public partial class Category
{
    public uint Id { get; set; }

    public string? Name { get; set; }

    [JsonIgnore]
    public virtual ICollection<Voucher> Vouchers { get; } = new List<Voucher>();
}
