﻿using coccan_api.Metadata;
using Microsoft.EntityFrameworkCore;
using coccan_api.Models;

namespace coccan_api.Models;

public partial class CoccanContext : DbContext
{
    public CoccanContext()
    {

    }

    public CoccanContext(DbContextOptions<CoccanContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Campaign> Campaigns { get; set; }

    public virtual DbSet<Category> Categories { get; set; }

    public virtual DbSet<Department> Departments { get; set; }

    public virtual DbSet<Group> Groups { get; set; }

    public virtual DbSet<Organization> Organizations { get; set; }

    public virtual DbSet<Scan> Scans { get; set; }

    public virtual DbSet<University> Universities { get; set; }

    public virtual DbSet<User> Users { get; set; }

    public virtual DbSet<Voucher> Vouchers { get; set; }

    public virtual DbSet<Wallet> Wallets { get; set; }

    public virtual DbSet<DeviceToken> DeviceTokens { get; set; }

    public virtual DbSet<UserDeviceToken> UserDeviceTokens { get; set; }
    public virtual DbSet<Transaction> Transactions { get; set; }

    public DbSet<Dictionary<string, object>> CampaignGroup => Set<Dictionary<string, object>>("CampaignGroup");
    public DbSet<Dictionary<string, object>> CampaignDepartment => Set<Dictionary<string, object>>("CampaignDepartment");



    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseMySql(GetConnectionString(), Microsoft.EntityFrameworkCore.ServerVersion.Parse("5.7-mysql")).UseSnakeCaseNamingConvention();

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .UseCollation("utf8mb3_general_ci")
            .HasCharSet("utf8mb3");

        modelBuilder.Entity<Campaign>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("campaign");

            entity.HasIndex(e => e.LecturerId, "campaign_lecturer_null_fk");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.ExpiredDate)
                .HasColumnType("datetime")
                .HasColumnName("expired_date");
            entity.Property(e => e.LecturerId).HasColumnName("lecturer_id");
            entity.Property(e => e.PointsGiven)
                .HasColumnType("int unsigned")
                .HasColumnName("points_given");
            entity.Property(e => e.Target)
                .HasColumnType("text")
                .HasColumnName("target");

            entity.HasOne(d => d.Lecturer).WithMany(p => p.Campaigns)
                .HasForeignKey(d => d.LecturerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("campaign_lecturer_null_fk");

            entity.HasMany(d => d.Departments).WithMany(p => p.Campaigns)
                .UsingEntity<Dictionary<string, object>>(
                    "CampaignDepartment",
                    r => r.HasOne<Department>().WithMany()
                        .HasForeignKey("DepartmentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .HasConstraintName("campaign_department_department_null_fk"),
                    l => l.HasOne<Campaign>().WithMany()
                        .HasForeignKey("CampaignId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .HasConstraintName("campaign_department_campaign_null_fk"),
                    j =>
                    {
                        j.HasKey("CampaignId", "DepartmentId")
                            .HasName("PRIMARY")
                            .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                        j.ToTable("campaign_department");
                        j.HasIndex(new[] { "DepartmentId" }, "campaign_department_department_null_fk");
                    });

            entity.HasMany(d => d.Groups).WithMany(p => p.Campaigns)
                .UsingEntity<Dictionary<string, object>>(
                    "CampaignGroup",
                    r => r.HasOne<Group>().WithMany()
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .HasConstraintName("campaign_group_group_null_fk"),
                    l => l.HasOne<Campaign>().WithMany()
                        .HasForeignKey("CampaignId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .HasConstraintName("campaign_group_campaign_null_fk"),
                    j =>
                    {
                        j.HasKey("CampaignId", "GroupId")
                            .HasName("PRIMARY")
                            .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                        j.ToTable("campaign_group");
                        j.HasIndex(new[] { "GroupId" }, "campaign_group_group_null_fk");
                    });
        });

        modelBuilder.Entity<Category>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("category");

            entity.Property(e => e.Id)
                .HasColumnType("int unsigned")
                .HasColumnName("id");
            entity.Property(e => e.Name)
                .HasColumnName("name");
        });

        modelBuilder.Entity<Department>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("department");

            entity.Property(e => e.Id)
                .HasColumnType("int unsigned")
                .HasColumnName("id");
            entity.Property(e => e.Name)
                .HasColumnType("text")
                .HasColumnName("name");
        });

        modelBuilder.Entity<Group>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("group");

            entity.HasIndex(e => e.Name, "group_name_idx").HasPrefixLength(10);

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Name)
                .HasColumnType("text")
                .HasColumnName("name");
        });


        modelBuilder.Entity<Organization>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("organization");

            entity.HasIndex(e => e.Name, "organization_name_idx");
            entity.Property(e => e.Id)
                .HasColumnType("bigint(20) unsigned")
                .HasColumnName("id");
            entity.Property(e => e.Address).HasColumnName("address");
            entity.Property(e => e.Logo)
                .HasColumnType("text")
                .HasColumnName("logo");
            entity.Property(e => e.Name).HasColumnName("name");
        });

        modelBuilder.Entity<Scan>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("scan");

            entity.HasIndex(e => e.CampaignId, "scan_campaign_null_fk");

            entity.HasIndex(e => e.StudentId, "scan_student_null_fk");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedAt)
                .HasDefaultValueSql("current_timestamp()")
                .HasColumnType("datetime")
                .HasColumnName("created_at");
            entity.Property(e => e.CampaignId).HasColumnName("campaign_id");
            entity.Property(e => e.Points)
                .HasColumnType("int unsigned")
                .HasColumnName("points");
            entity.Property(e => e.StudentId).HasColumnName("student_id");

            entity.HasOne(d => d.Campaign).WithMany(p => p.Scans)
                .HasForeignKey(d => d.CampaignId)
                .HasConstraintName("scan_campaign_null_fk");

            entity.HasOne(d => d.Student).WithMany(p => p.Scans)
                .HasForeignKey(d => d.StudentId)
                .HasConstraintName("scan_student_null_fk");
        });


        modelBuilder.Entity<University>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("university");

            entity.Property(e => e.Id)
                .HasColumnType("int(10) unsigned")
                .HasColumnName("id");
            entity.Property(e => e.Name)
                .HasColumnType("text")
                .HasColumnName("name");
            entity.HasMany(d => d.Departments).WithMany(p => p.Universities)
                .UsingEntity<Dictionary<string, object>>(
                    "DepartmentUniversity",
                    r => r.HasOne<Department>().WithMany()
                        .HasForeignKey("DepartmentId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("department_university_department_null_fk"),
                    l => l.HasOne<University>().WithMany()
                        .HasForeignKey("UniversityId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("department_university_university_null_fk"),
                    j =>
                    {
                        j.HasKey("DepartmentId", "UniversityId")
                            .HasName("PRIMARY")
                            .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                        j.ToTable("department_university");
                        j.HasIndex(new[] { "DepartmentId" }, "department_university_department_null_fk");
                    });
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("user");

            entity.HasIndex(e => e.DepartmentId, "user_department_null_fk");

            entity.HasIndex(e => e.UniversityId, "user_university_null_fk");

            entity.HasIndex(e => e.Username, "user_username_idx").HasPrefixLength(10);

            entity.HasIndex(e => e.Email, "user_email_idx").HasPrefixLength(20);

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.DepartmentId)
                .HasColumnType("int(10) unsigned")
                .HasColumnName("department_id").HasDefaultValue((uint)1);
            entity.Property(e => e.Email).HasColumnName("email");
            entity.Property(e => e.ProfilePhoto).HasColumnName("profile_photo");
            entity.Property(e => e.Role).HasColumnName("role").HasConversion<string>().HasDefaultValue(Role.STUDENT);
            entity.Property(e => e.UniversityId)
                .HasColumnType("int(10) unsigned")
                .HasColumnName("university_id").HasDefaultValue((uint)1);
            entity.Property(e => e.Username).HasColumnName("username");

            entity.HasOne(d => d.Department).WithMany(p => p.Users)
                .HasForeignKey(d => d.DepartmentId)
                .HasConstraintName("user_department_null_fk");

            entity.HasOne(d => d.University).WithMany(p => p.Users)
                .HasForeignKey(d => d.UniversityId)
                .HasConstraintName("user_university_null_fk");
        });

        modelBuilder.Entity<Voucher>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("voucher");

            entity.HasIndex(e => e.CategoryId, "voucher_category_null_fk");

            entity.HasIndex(e => e.OrganizationId, "voucher_organization_null_fk");

            entity.HasIndex(e => e.CreatedDate, "voucher_created_date_idx");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Address)
                .HasColumnType("text")
                .HasColumnName("address");
            entity.Property(e => e.CategoryId)
                .HasColumnType("int unsigned")
                .HasColumnName("category_id");
            entity.Property(e => e.Description)
                .HasColumnType("text")
                .HasColumnName("description");
            entity.Property(e => e.ExpiredDate)
                .HasColumnType("datetime")
                .HasColumnName("expired_date");
            entity.Property(e => e.Number)
                .HasColumnType("int unsigned")
                .HasColumnName("number");
            entity.Property(e => e.OrganizationId)
                .HasColumnType("bigint(20) unsigned")
                .HasColumnName("organization_id");

            entity.HasOne(d => d.Category).WithMany(p => p.Vouchers)
                .HasForeignKey(d => d.CategoryId)
                .HasConstraintName("voucher_category_null_fk");

            entity.HasOne(d => d.Organization).WithMany(p => p.Vouchers)
                .HasForeignKey(d => d.OrganizationId)
                .HasConstraintName("voucher_organization_null_fk");
        });

        modelBuilder.Entity<Wallet>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("wallet");

            entity.HasIndex(e => e.UserId, "wallet_user_null_fk");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.TotalPoints)
                .HasColumnType("int unsigned")
                .HasColumnName("total_points");
            entity.Property(e => e.UserId).HasColumnName("user_id");
            entity.Property(e => e.WalletType)
                .HasColumnType("text")
                .HasColumnName("wallet_type");

            entity.HasOne(d => d.User).WithMany(p => p.Wallets)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("wallet_user_null_fk");
        });

        modelBuilder.Entity<DeviceToken>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("device_token");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Code)
                .HasColumnName("code");
        });

        modelBuilder.Entity<UserDeviceToken>(entity =>
        {
            entity.HasKey(e => new { e.UserId, e.DeviceTokenId})
                .HasName("PRIMARY")
                .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

            entity.ToTable("user_device_token");

            entity.HasIndex(e => e.DeviceTokenId, "user_device_token_device_token_null_fk");

            entity.Property(e => e.UserId).HasColumnName("user_id");
            entity.Property(e => e.DeviceTokenId).HasColumnName("device_token_id");
            entity.Property(e => e.IsLoggedIn)
            .HasColumnType("tinyint(1)")
                .HasColumnName("is_logged_in");

            entity.HasOne(d => d.User).WithMany(p => p.UserDeviceTokens)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("user_device_token_user_null_fk");

            entity.HasOne(d => d.DeviceToken).WithMany(p => p.UserDeviceTokens)
                .HasForeignKey(d => d.DeviceTokenId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("user_device_token_device_token_null_fk");
        });

        modelBuilder.Entity<GroupStudent>(entity =>
        {
            entity.HasKey(e => new { e.GroupId, e.StudentId})
                .HasName("PRIMARY")
                .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

            entity.ToTable("group_student");

            entity.HasIndex(e => e.GroupId, "group_student_group_null_fk");

            entity.Property(e => e.GroupId).HasColumnName("group_id");
            entity.Property(e => e.StudentId).HasColumnName("student_id");

            entity.HasOne(d => d.Group).WithMany(p => p.GroupStudents)
                .HasForeignKey(d => d.GroupId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("group_student_group_null_fk");

            entity.HasOne(d => d.Student).WithMany(p => p.GroupStudents)
                .HasForeignKey(d => d.StudentId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("group_student_student_null_fk");
        });

        modelBuilder.Entity<Transaction>(entity =>
        {
            entity.HasKey(e => e.Id)
                .HasName("PRIMARY");

            entity.ToTable("transaction");

            entity.HasIndex(e => e.WalletId, "transaction_wallet_null_fk");
            entity.HasIndex(e => e.VoucherId, "transaction_voucher_null_fk");

            entity.Property(e => e.VoucherId).HasColumnName("voucher_id");
            entity.Property(e => e.Item).HasColumnName("item");
            entity.Property(e => e.CreatedDatetime).HasColumnType("datetime").HasColumnName("created_datetime");
            entity.Property(e => e.Description).HasColumnName("description");
            entity.Property(e => e.Points).HasColumnType("int").HasColumnName("points");

            entity.HasOne(d => d.Wallet).WithMany(p => p.Transactions)
                .HasForeignKey(d => d.WalletId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("transaction_wallet_null_fk");

            entity.HasOne(d => d.Voucher).WithMany(p => p.Transactions)
                .HasForeignKey(d => d.VoucherId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("transaction_voucher_null_fk");
        });


        modelBuilder.SharedTypeEntity<Dictionary<string, object>>(
            "CampaignGroup", cg =>
            {
                cg.IndexerProperty<Guid>("CampaignId");
                cg.IndexerProperty<Guid>("GroupId");
            });
        modelBuilder.SharedTypeEntity<Dictionary<string, object>>(
            "CampaignDepartment", cd =>
            {
                cd.IndexerProperty<Guid>("CampaignId");
                cd.IndexerProperty<uint>("DepartmentId");
            });

        //modelBuilder.Entity<CampaignDepartment>(entity =>
        //{
        //    entity.HasKey(e => new { e.CampaignId, e.De })
        //        .HasName("PRIMARY")
        //        .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

        //    entity.ToTable("group_student");

        //    entity.HasIndex(e => e.GroupId, "group_student_group_null_fk");

        //    entity.Property(e => e.GroupId).HasColumnName("group_id");
        //    entity.Property(e => e.StudentId).HasColumnName("student_id");

        //    entity.HasOne(d => d.Group).WithMany(p => p.GroupStudents)
        //        .HasForeignKey(d => d.GroupId)
        //        .OnDelete(DeleteBehavior.Cascade)
        //        .HasConstraintName("group_student_group_null_fk");

        //    entity.HasOne(d => d.Student).WithMany(p => p.GroupStudents)
        //        .HasForeignKey(d => d.StudentId)
        //        .OnDelete(DeleteBehavior.ClientSetNull)
        //        .HasConstraintName("group_student_student_null_fk");
        //})

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

    private string GetConnectionString()
    {
        IConfiguration config = new ConfigurationBuilder()
         .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", true, true)
        .Build();
        var strConn = config["ConnectionStrings:CoccanDB"];

        return strConn;
    }

    public DbSet<coccan_api.Models.GroupStudent> GroupStudent { get; set; }
}
