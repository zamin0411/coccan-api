﻿using Newtonsoft.Json;

namespace coccan_api.Models;

public partial class Voucher
{
    public Guid Id { get; set; }

    public DateTime ExpiredDate { get; set; }

    public uint? Number { get; set; }

    public string? Address { get; set; }

    public ulong? OrganizationId { get; set; }

    public string? Description { get; set; }

    public uint? CategoryId { get; set; }

    public DateTime CreatedDate { get; set; }

    public uint RequiredPoints { get; set; }

    public virtual Category? Category { get; set; }

    public virtual Organization? Organization { get; set; }
    [JsonIgnore]
    public ICollection<Transaction> Transactions { get; set; }
}

public record PostVoucherDTO
{
    public ulong OrganizationId { get; set; }
    public uint CategoryId { get; set; }
    
    public string? Description { get; set; }
    public string Address { get; set; }
    public uint Number { get; set; }

    public DateTime ExpiredDate { get; set; }

    public uint RequiredPoints { get; set; }


}
