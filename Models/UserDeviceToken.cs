﻿namespace coccan_api.Models
{
    public partial class UserDeviceToken
    {
        public Guid UserId { get; set; }
        public Guid DeviceTokenId { get; set; }
        public bool IsLoggedIn { get; set; } = true;

        public User User { get; set; } = null!;

        public DeviceToken DeviceToken { get; set; }


    }

}
