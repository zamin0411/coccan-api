﻿

namespace coccan_api.Models
{
    public partial class DeviceToken
    {

        public Guid Id { get; set; }
        public string? Code { get; set; }

        public virtual UserDeviceTokenList UserDeviceTokens { get; } = new UserDeviceTokenList();


    }

    public record LoginBody
    {

        public string RegistrationToken { get; set; }
    }
}
