﻿namespace coccan_api.Models
{
    public partial class Message
    {
        public Guid Id { get; set; }

        public string? Content { get; set; }

        public Guid GroupId { get; set; }

        public DateTime CreatedDateTime { get; set; }
    }
}
