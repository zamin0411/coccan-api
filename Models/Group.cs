﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace coccan_api.Models;

public partial class Group
{
    public Guid Id { get; set; }

    public string? Name { get; set; }

    public string Password { get; set; }

    public string? Description { get; set; }

    public string? ZaloLink { get; set; }

    public Guid LecturerId { get; set; }

    public User? Lecturer { get; set; }
    [JsonIgnore]

    public virtual ICollection<Campaign>? Campaigns { get; } = new List<Campaign>();

    public virtual ICollection<Message>? Messages { get; } = new List<Message>();

    [JsonIgnore]
    public ICollection<GroupStudent>? GroupStudents { get; } = new List<GroupStudent>();
}

public record GroupResponseDTO
{
    public Guid Id { get; set; }

    public string? Name { get; set; }

    public string? Password { get; set; }

    public string? Description { get; set; }

    public string? ZaloLink { get; set; }

    public User Lecturer { get; set; }

    public virtual ICollection<Message> Messages { get; } = new List<Message>();

    public ICollection<User> Students { get; } = new List<User>();


}

public record PostGroupDTO
{
    public string Name { get; set; }

    public string Password { get; set; }

    public string? Description { get; set; }

    public string? ZaloLink { get; set; }

    public Guid LecturerId { get; set; }
}
