﻿using System;
using System.Collections.Generic;

namespace coccan_api.Models;

public partial class Lecturer
{
    public Guid UserId { get; set; }

    public virtual ICollection<Campaign> Campaigns { get; } = new List<Campaign>();

    public virtual ICollection<Scan> Scans { get; } = new List<Scan>();

    public virtual User User { get; set; } = null!;

    public virtual ICollection<Group> Groups { get; } = null!;
}
