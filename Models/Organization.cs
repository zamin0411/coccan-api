﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Policy;


namespace coccan_api.Models;

public partial class Organization
{
    public ulong Id { get; set; }

    public string? Name { get; set; }

    public string? Logo { get; set; }

    [JsonIgnore]
    public string? Address { get; set; }

    public string? Description { get; set; }
    [JsonIgnore]
    public virtual ICollection<Voucher> Vouchers { get; } = new List<Voucher>();
}

public record PostOrganizationDTO{
    public string Name { get; set; }
    public IFormFile Logo { get; set; }
    public string Description { get; set; }

}

public record PutOrganizationDTO
{
    public string Name { get; set; }

    public string LogoLink { get; set; }
    public IFormFile? Logo { get; set; }
    public string Description { get; set; }

}
