﻿namespace coccan_api.Models
{
    public partial class GroupStudent
    {
        public Guid GroupId { get; set; }
        public Guid StudentId { get; set; }
        public User Student { get; set; } = null!;
        public Group Group { get; set; } = null!;
    }

    public record PostGroupStudentDTO
    {
        public Guid GroupId { get; set; }
        public Guid StudentId { get; set; }

    }
}
