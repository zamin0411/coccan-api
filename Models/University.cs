﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace coccan_api.Models;

public partial class University
{
    public uint Id { get; set; }

    public string? Name { get; set; }

    [JsonIgnore]
    public virtual ICollection<User> Users { get; } = new List<User>();
    [JsonIgnore]
    public virtual ICollection<Department> Departments { get; } = new List<Department>();

}

public record PostUniversityDTO
{
    public string? Name { get; set; }
    public ICollection<Department>? Departments { get; set; }

}
