﻿using System;
using System.Collections.Generic;

namespace coccan_api.Models;

public partial class Student
{
    public Guid UserId { get; set; }

    public string? Code { get; set; }

    public virtual ICollection<Scan> Scans { get; } = new List<Scan>();

    public virtual User User { get; set; } = null!;

    public virtual ICollection<Group> Groups { get; } = new List<Group>();
}
