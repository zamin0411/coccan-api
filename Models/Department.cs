﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace coccan_api.Models;

public partial class Department
{
    public uint Id { get; set; }

    public string? Name { get; set; }

    [JsonIgnore]
    public ICollection<User> Users { get; } = new List<User>();

    [JsonIgnore]
    public ICollection<Campaign> Campaigns { get; } = new List<Campaign>();
    [JsonIgnore]
    public ICollection<University> Universities { get; } = new List<University>();

}
