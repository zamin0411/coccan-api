﻿
using Newtonsoft.Json;


namespace coccan_api.Models;

public partial class User
{
    public Guid Id { get; set; }

    public string Username { get; set; }

    public string Email { get; set; }

    public string? ProfilePhoto { get; set; }

    public uint DepartmentId { get; set; }

    public uint UniversityId { get; set; }

    public virtual Department? Department { get; set; }
    public string Role { get; set; }

    public virtual University? University { get; set; }
    
    public virtual ICollection<Wallet> Wallets { get; } = new List<Wallet>();

    [JsonIgnore]
    public virtual UserDeviceTokenList? UserDeviceTokens { get; } = new UserDeviceTokenList();
    [JsonIgnore]
    public virtual ICollection<Campaign>? Campaigns { get; } = null!;
    [JsonIgnore]
    public virtual ICollection<Scan>? Scans { get; set; } = null!;
    [JsonIgnore]
    public ICollection<GroupStudent>? GroupStudents { get; } = null!;

}

public record PostUserDTO
{
    public string Role { get; set; }
    public string Username { get; set; }
    public string Email { get; set; }
    public string? ProfilePhoto { get; set; }
    public uint DepartmentId { get; set; }
    public uint UniversityId { get; set; }

}

public record PatchUserDTO
{
    public string? Role { get; set; }
    public string? Username { get; set; }
    public string? Email { get; set; }
    public string? ProfilePhoto { get; set; }
    public uint? DepartmentId { get; set; }
    public uint? UniversityId { get; set; }

}


public record AuthResponseDTO
{ 
    public string? Token { get; set;}
}

public record UserResponseDTO
{
    public Guid Id { get; set; }
    public string Role { get; set; }

    public string Username { get; set; }

    public string Email { get; set; }

    public string? ProfilePhoto { get; set; }

    public Department Department { get; set; }

    public University University { get; set; }

    public ICollection<Wallet> Wallets { get; } = new List<Wallet>();
}

public record LecturerResponseDTO
{
    public string Username { get; set; }
    public string Email { get; set; }
    public string? ProfilePhoto { get; set; }

}

public record UserVoucherResponseDTO
{
    public Guid UserId { get; set; }
    public Voucher Voucher { get; set; }
    public string Item { get; set; }
}

public record UserTransactionResponseDTO
{
    public Guid Id { get; set; }

    public string Description { get; set; }
    public uint Points { get; set; }
    public DateTime CreatedDatetime { get; set; }

    public string Item { get; set; }
}
