﻿using coccan_api.Models;
using coccan_api.Redis;
using coccan_api.Repositories;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;


namespace coccan_api.Services
{
    public interface IVoucherService
    {
        Task BackgroundCacheVouchers();
        Task BackgroundCacheVouchers(string key, IEnumerable<Voucher> vouchers);

        Task BackgroundCacheVoucher(Voucher voucher);

        Task<IEnumerable<Voucher>> GetAsync(DateTime? after, int size);

        Task<IEnumerable<Voucher>> GetRelatedDataAsync();
    }
    public class VoucherService : IVoucherService
    {
        private readonly CoccanContext _context;
        private readonly RedisConnection _redis;
        private readonly IVoucherRepository _voucherRepository;

        public VoucherService(CoccanContext context, RedisConnection redis, IVoucherRepository voucherRepository)
        {
            _context = context;
            _redis = redis;
            _voucherRepository = voucherRepository;
        }

        public async Task BackgroundCacheVoucher(Voucher voucher)
        {
            await _redis.BasicRetryAsync(async (db) => await db.StringSetAsync("voucher " + voucher.Id.ToString(), JsonConvert.SerializeObject(voucher), new TimeSpan(1, 0, 0)));
        }

        public async Task BackgroundCacheVouchers()
        {
            var vouchers = await _voucherRepository.GetRelatedDataAsync();
            await _redis.BasicRetryAsync(async (db) => await db.StringSetAsync("vouchers", JsonConvert.SerializeObject(vouchers), new TimeSpan(1, 0, 0)));
        }

        public async Task BackgroundCacheVouchers(string key, IEnumerable<Voucher> vouchers)
        {

            await _redis.BasicRetryAsync(async (db) => await db.StringSetAsync(key, JsonConvert.SerializeObject(vouchers), new TimeSpan(1, 0 , 0)));
        }

        public async Task<IEnumerable<Voucher>> GetAsync(DateTime? after, int size)
        {
            return await _voucherRepository.GetAsync(after, size);
        }

        public async Task<IEnumerable<Voucher>> GetRelatedDataAsync()
        {
            return await _voucherRepository.GetRelatedDataAsync();
        }

    }
}
