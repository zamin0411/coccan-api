﻿using coccan_api.Models;

namespace coccan_api.Services
{
    public interface IUserService
    {
        Task<Guid> CreateNewUser(User user);
        Task<Guid> RegisterNewMobileUser(User user, DeviceToken deviceToken);
    }
}
