﻿using AutoMapper;
using coccan_api.Metadata;
using coccan_api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace coccan_api.Services
{
    public class UserService: IUserService
    {
        private readonly CoccanContext _context;
        public UserService(CoccanContext context) {
            _context = context;
        }

        public async Task<Guid> CreateNewUser(User user)
        {
            if (user.Role.ToUpper() != Role.ADMIN.ToString())
            {
                Wallet wallet = new Wallet
                {
                    TotalPoints = 0,
                    WalletType = WalletType.TOEXCHANGE.ToString()

                };
                user.Wallets.Add(wallet);
            }
            if (user.Role.ToUpper() == Role.LECTURER.ToString())
            {
                Wallet wallet = new Wallet
                {
                    TotalPoints = 0,
                    WalletType = WalletType.TOGIVE.ToString()

                };
                user.Wallets.Add(wallet);
            }
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
            return user.Id;
        }

        public async Task<Guid> RegisterNewMobileUser(User user, DeviceToken deviceToken)
        {
            if (user.Role.ToUpper() != Role.ADMIN.ToString())
            {
                Wallet wallet = new Wallet
                {
                    TotalPoints = 0,
                    WalletType = WalletType.TOEXCHANGE.ToString()

                };
                user.Wallets.Add(wallet);
            }
            if (user.Role.ToUpper() == Role.LECTURER.ToString())
            {
                Wallet wallet = new Wallet
                {
                    TotalPoints = 0,
                    WalletType = WalletType.TOGIVE.ToString()

                };
                user.Wallets.Add(wallet);
            }
            var userDeviceToken = new UserDeviceToken { User = user, DeviceToken = deviceToken };
            await _context.UserDeviceTokens.AddAsync(userDeviceToken);
            await _context.SaveChangesAsync();
            return user.Id;
        }
    }
}
