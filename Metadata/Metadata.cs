﻿namespace coccan_api.Metadata
{
    public enum Role
    {
        ADMIN,
        LECTURER,
        STUDENT
    }

    public enum WalletType
    {
        TOGIVE,
        TOEXCHANGE
    }

    public enum CampaignTarget
    {
        DEPARTMENT,
        GROUP,
        ALL
    }
}
