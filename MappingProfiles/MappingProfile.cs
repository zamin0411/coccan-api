﻿using AutoMapper;
using coccan_api.Models;

namespace coccan_api.MappingProfiles
{
    public class MappingProfile: Profile
    {
        public MappingProfile() {

            // User
            CreateMap<PostUserDTO, User>();
            CreateMap<User, UserResponseDTO>();
            CreateMap<LoginBody, DeviceToken>();
            CreateMap<User, LecturerResponseDTO>();

            // University
            CreateMap<PostUniversityDTO, University>();

            // Scan
            CreateMap<PostScanDTO, Scan>();

            // GroupStudent
            CreateMap<PostGroupStudentDTO, GroupStudent>();

            // Group
            CreateMap<PostGroupDTO, Group>();
            CreateMap<Group, GroupResponseDTO>().ForMember(dto => dto.Students, opt => opt.MapFrom(x => x.GroupStudents.Select(y => y.Student).ToList()));

            // WalletHistory
            CreateMap<PostTransactionDTO, Transaction>();
            
            // Voucher
            CreateMap<PostVoucherDTO, Voucher>();

            // Campaign
            CreateMap<PostCampaignDTO, Campaign>();
            //CreateMap<PostCampaignDTO, Campaign>().ForMember(c => c.Groups, opt => opt.MapFrom(g => CreateGroup(g.Groups))).ForMember(c => c.Departments, opt => opt.MapFrom(g => CreateDepartment(g.Departments)));
            CreateMap<Campaign, CampaignResponseDTO>();

            //VoucherUser
            CreateMap<Transaction, UserVoucherResponseDTO>().ForMember(dto => dto.UserId, opt => opt.MapFrom(t => t.Wallet.UserId));

            //Scan
            CreateMap<Scan, ScanResponseDTO>();    

        }

        private static List<Group> CreateGroup(IList<Guid>? groupsId)
        {
            IList<Group> groups = new List<Group>();
            if (groupsId != null)
            {
                foreach (Guid groupId in groupsId)
                {
                    groups.Add(new Group
                    {
                        Id = groupId
                    });
                }
            }
            return groups.ToList();
        }

        private static List<Department> CreateDepartment(IList<uint>? departmentsId)
        {
            IList<Department> departments = new List<Department>();
            if (departmentsId != null)
            {
                foreach (uint departmentId in departmentsId)
                {
                    departments.Add(new Department
                    {
                        Id = departmentId
                    });
                }
            }
            return departments.ToList();
        }
    }
}
